export const environment = {
  production: true,
  basePokeUrl: "https://pokeapi.co/api/v2",
  frontDefaultSpriteUrl: (id: number) => `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`,
  frontShinySpriteUrl: (id: number) => `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/${id}.png`,
  pageSize: 20
};
