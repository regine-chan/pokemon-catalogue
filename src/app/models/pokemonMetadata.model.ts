export interface PokemonMetadata {
    id: number,
    name: string,
    imgSrc: string
}