import { TestObject } from 'protractor/built/driverProviders';
import { Deserializable } from "./deserializable.model";

export class Pokemon implements Deserializable {

    id?: number;
    name: string;
    url: string;
    abilities?: any[];
    base_experience?: number;
    forms?: any;
    moves?: any[];
    sprites?: {
        back_female: string,
        back_shiny_female: string,
        back_default: string,
        front_female: string,
        front_shiny_female: string,
        back_shiny: string,
        front_default: string,
        front_shiny: string,
    };
    stats?: any[];
    height?: number;
    weight?: number;
    types?: any[];

    deserialize(input: any): this {
        return Object.assign(this, input);
    };
}
