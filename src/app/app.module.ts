import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './components/pages/landing/landing.component';
import { TrainerComponent } from './components/pages/trainer/trainer.component';
import { CatalogueComponent } from './components/pages/catalogue/catalogue.component';
import { PokemonComponent } from './components/pages/pokemon/pokemon.component';
import { PokememonHeaderComponent } from './components/pokememon-header/pokememon-header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TrainerCardComponent } from './components/cards/trainer-card/trainer-card.component';
import { HttpClientModule } from '@angular/common/http';
import { PokemonCardComponent } from './components/cards/pokemon-card/pokemon-card.component';


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    TrainerComponent,
    CatalogueComponent,
    PokemonComponent,
    PokememonHeaderComponent,
    TrainerCardComponent,
    PokemonCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
