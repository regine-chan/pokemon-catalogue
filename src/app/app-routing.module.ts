import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './components/pages/landing/landing.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: 'landing',
    component: LandingComponent
  },
  {
    path: 'catalogue/:offset',
    loadChildren: () => import('./components/pages/catalogue/catalogue.module').then(m => m.CatalogueModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'pokemon/:pokemonId',
    loadChildren: () => import('./components/pages/pokemon/pokemon.module').then(m => m.PokemonModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'trainer',
    loadChildren: () => import('./components/pages/trainer/trainer.module').then(m => m.TrainerModule),
    canActivate: [AuthGuard]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: "/landing"
  },
  {
    path: '**',
    redirectTo: "/landing"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
