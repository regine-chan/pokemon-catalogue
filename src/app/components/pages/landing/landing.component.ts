import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent {

  trainerForm: FormGroup = new FormGroup({
    trainerName: new FormControl("", [Validators.required])
  });

  constructor(private session: SessionService, private router: Router) {
    // Routes the user to trainer page if they have a user stored
    if (session.get()) {
      this.router.navigateByUrl("/trainer")
    }
  }

  // Fetches trainer name from the form
  get trainerName() {
    return this.trainerForm.get("trainerName");
  }

  // saves the trainer and navigates to the trainer page
  onSubmitClicked() {
    this.session.save(this.trainerForm.value);
    this.router.navigateByUrl("/trainer");
  }

}
