import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonMetadata } from 'src/app/models/pokemonMetadata.model';
import { SessionService } from 'src/app/services/session/session.service';
import { TrainerCollectionService } from 'src/app/services/trainerCollection/trainer-collection.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss']
})
export class TrainerComponent implements OnInit {

  pokemonMetadata: PokemonMetadata[] = [];

  constructor(private trainerCollectionService: TrainerCollectionService, private router: Router) {
    // Subscribes to trainer collection
    this.trainerCollectionService.trainerCollection.subscribe(pokemonMetadata => {
      this.pokemonMetadata = pokemonMetadata;
    })
  }

  ngOnInit(): void {
  }

  // Navigates to the pokemon detail page
  handleClickPokemon(value) {
    this.router.navigate(["/pokemon", value]);
  }



}
