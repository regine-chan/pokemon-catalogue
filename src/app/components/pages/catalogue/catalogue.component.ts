import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PokemonMetadata } from 'src/app/models/pokemonMetadata.model';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit {


  public offset: number;
  public pokemonMetadata: PokemonMetadata[] = [];

  constructor(private pokemonService: PokemonService, private route: ActivatedRoute, private router: Router) {
    // Subscribes to param changes, to fetch more pokemon
    this.route.paramMap.subscribe(params => {
      this.offset = Number(params.get("offset"));
      this.updatePokemon();
    });
  }

  // Fetches pokemon
  async ngOnInit() {
    this.pokemonMetadata = await this.pokemonService.getPokemon(this.offset);
  }

  // Updates pokemon list
  async updatePokemon() {
    this.pokemonMetadata = await this.pokemonService.getPokemon(this.offset);
  }

  // Routes user to pokemon detail page
  handleClickPokemon(pokemonId: number) {
    this.router.navigate(["/pokemon", pokemonId]);
  }

  // Handles pagination incrementation and belov 0 cases
  handlePrevClicked() {
    if (this.offset <= 0) {
      this.router.navigate(["/catalogue", 1030]);
    } else {
      this.router.navigate(["/catalogue", this.offset - environment.pageSize]);
    }
  }

  // Handles pagination incrementation and above 1050 cases
  handleNextClicked() {
    if (this.offset >= 1030) {
      this.router.navigate(["/catalogue", 0]);
    } else {
      this.router.navigate(["/catalogue", this.offset + environment.pageSize]);
    }
  }




}
