import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';
import { TrainerCollectionService } from 'src/app/services/trainerCollection/trainer-collection.service';

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss']
})
export class PokemonComponent {

  private pokemonId: number = 0;

  private pokemon$: Subscription;

  public pokemon: Pokemon = null;

  constructor(private pokemonService: PokemonService, private route: ActivatedRoute, private router: Router, private trainerCollection: TrainerCollectionService) {
    // Subscribes to route params
    this.route.paramMap.subscribe(params => {
      this.pokemonId = Number(params.get("pokemonId"));
      this.getPokemon();
    });
  }


  // Fetches pokemon
  async getPokemon() {
    this.pokemon = await this.pokemonService.getPokemonById(this.pokemonId);
  }

  // Prettyfies pokemon id
  pokemonIdToString() {
    if (this.pokemon.id < 10) {
      return "#00" + this.pokemon.id;
    } else if (this.pokemon.id < 100) {
      return "#0" + this.pokemon.id;
    } else {
      return "#" + this.pokemon.id;
    }
  }

  // Adds pokemon to trainer collection
  handleCollectPokemon() {
    this.trainerCollection.addToCollection({
      id: this.pokemon.id,
      name: this.pokemon.name,
      imgSrc: this.pokemon.sprites.front_default
    });
  }

  // Removes pokemon from trainer collection
  handleRemovePokemon() {
    this.trainerCollection.removeFromCollection(this.pokemon.id);
  }

  // Checks if pokemon is collected
  hasCollected(): boolean {
    return this.trainerCollection.exists(this.pokemon.id);
  }

  // Loads the prev pokemon by id - 1
  getPrevPokemon(): void {
    if (this.pokemon.id == 0) {
      this.router.navigate(["/pokemon", 1050]);
    }
    this.router.navigate(["/pokemon", this.pokemon.id - 1]);
  }

  // Loads the next pokemon by id + 1
  getNextPokemon(): void {
    if (this.pokemon.id == 1050) {
      this.router.navigate(["/pokemon", 0]);
    }
    this.router.navigate(["/pokemon", this.pokemon.id + 1]);
  }


}
