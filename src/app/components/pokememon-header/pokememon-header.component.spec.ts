import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokememonHeaderComponent } from './pokememon-header.component';

describe('PokememonHeaderComponent', () => {
  let component: PokememonHeaderComponent;
  let fixture: ComponentFixture<PokememonHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokememonHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokememonHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
