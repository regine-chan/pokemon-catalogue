import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-trainer-card',
  templateUrl: './trainer-card.component.html',
  styleUrls: ['./trainer-card.component.scss']
})
export class TrainerCardComponent implements OnInit {

  constructor(private session: SessionService, private router: Router) { }

  ngOnInit(): void {
  }

  get trainerName(): string {
    return this.session.get().trainerName;
  }

  // Removes stored application data permanently and navigates back to landing page
  handleLogout() {
    this.session.remove();
    this.router.navigate(["/landing"]);
  }


}
