import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss']
})
export class PokemonCardComponent implements OnInit {

  @Input() pokemon;
  @Output() clickPokemon: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onPokemonClicked() {
    this.clickPokemon.emit(this.pokemon.id);
  }

  // Makes a pretty string to display pokemon id 
  pokemonIdToString() {
    if (this.pokemon.id < 10) {
      return "#00" + this.pokemon.id;
    } else if (this.pokemon.id < 100) {
      return "#0" + this.pokemon.id;
    } else {
      return "#" + this.pokemon.id;
    }
  }

}
