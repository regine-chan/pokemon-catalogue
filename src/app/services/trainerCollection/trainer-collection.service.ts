import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonMetadata } from 'src/app/models/pokemonMetadata.model';

@Injectable({
  providedIn: 'root'
})
export class TrainerCollectionService {

  public trainerCollection: BehaviorSubject<PokemonMetadata[]> = new BehaviorSubject<PokemonMetadata[]>([]);

  constructor() {
    const exists = localStorage.getItem('TC');
    if (exists) {
      this.trainerCollection.next(JSON.parse(exists));
    }
  }

  // Saves collection to localstorage
  save() {
    localStorage.setItem('TC', JSON.stringify(this.trainerCollection.value));
  }

  // Checks if pokemon exist in collection
  exists(pokemonId: number): boolean {
    return this.trainerCollection.value.find(pm => pm.id === pokemonId) !== undefined;
  }

  // Adds pokemon to collection if it doesn't already exist
  addToCollection(pokemonMetadata: PokemonMetadata) {
    const currentCollection = [... this.trainerCollection.value];

    if (!this.exists(pokemonMetadata.id)) {
      currentCollection.push(pokemonMetadata);
      this.trainerCollection.next(currentCollection);
      this.save();
    }
  }

  // Removes a pokemon from the collection if they exist
  removeFromCollection(pokemonId: number) {
    const currentCollection = [... this.trainerCollection.value];

    if (this.exists(pokemonId)) {
      this.trainerCollection.next(currentCollection.filter(p => p.id !== pokemonId));
      this.save();
    }
  }


}
