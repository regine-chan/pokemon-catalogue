import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map, catchError, switchMap } from 'rxjs/operators';
import { BehaviorSubject, throwError } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { PokemonMetadata } from 'src/app/models/pokemonMetadata.model';
import { Pokemon } from 'src/app/models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  public pokemon$: BehaviorSubject<Pokemon> = new BehaviorSubject<Pokemon>(null);

  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  // Fetches 20 pokemon from the pokemon api
  getPokemon(offset: number): Promise<PokemonMetadata[]> {
    return this.http.get<PokemonMetadata>(`${environment.basePokeUrl}/pokemon/?limit=${environment.pageSize}&offset=${offset}`)
      .pipe(
        map((response: any) => response.results),
        map(results => {
          return results.map((pokemon, index) => {
            return {
              id: offset + index + 1,
              name: pokemon.name,
              imgSrc: environment.frontDefaultSpriteUrl(parseInt(offset + index + 1))
            };
          })
        }),
        catchError(error => {
          if (error.status === 404) {
            return throwError('Could not find any pokemon');
          }

          if (error.status === 500) {
            return throwError('Service currently unavailable, please try again later');
          }

          return throwError(error);
        })
      )
      .toPromise();
  }

  // Fetches a pokemons details by id
  getPokemonById(pokemonId: number): Promise<Pokemon> {
    return this.http.get(`${environment.basePokeUrl}/pokemon/${pokemonId}`)
      .pipe(
        map(data => new Pokemon().deserialize(data))
      )
      .toPromise();
  }

}
