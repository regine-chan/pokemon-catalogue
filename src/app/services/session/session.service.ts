import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  // Saves the user
  save(session: any) {
    localStorage.setItem('sp_session', JSON.stringify(session));
  }

  // Fetches a user if they exist
  get(): any {
    const savedSession = localStorage.getItem('sp_session');
    return savedSession ? JSON.parse(savedSession) : false;
  }

  // Removes all local storage
  remove() {
    localStorage.clear();
  }
}
